<div class="modal fade car-modal" id="carModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <svg width="27" height="26" viewBox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="23" width="5" height="31" transform="rotate(45 23 0)" fill="white"/>
                        <path d="M26 21.9199L22.4645 25.4555L0.544156 3.53515L4.07969 -0.000387967L26 21.9199Z" fill="white"/>
                    </svg>
                </button>
                <h4 class="modal-title block-title" id="myModalLabel">Полный список машин Uber</h4>
            </div>
            <div class="modal-body">
                <div class="alphabet-wrapper">
                    <a href="#" class="scroll-to">a</a>
                    <a href="#" class="scroll-to">b</a>
                    <a href="#" class="scroll-to">c</a>
                    <a href="#" class="scroll-to">d</a>
                    <a href="#" class="disable">e</a>
                    <a href="#" class="scroll-to">f</a>
                    <a href="#" class="scroll-to">g</a>
                    <a href="#" class="scroll-to">h</a>
                    <a href="#" class="scroll-to">i</a>
                    <a href="#" class="scroll-to">j</a>
                    <a href="#" class="scroll-to">k</a>
                    <a href="#" class="scroll-to">l</a>
                    <a href="#" class="scroll-to">n</a>
                    <a href="#" class="scroll-to">m</a>
                    <a href="#" class="scroll-to">o</a>
                    <a href="#" class="scroll-to">p</a>
                    <a href="#" class="scroll-to">q</a>
                    <a href="#" class="scroll-to">r</a>
                    <a href="#" class="scroll-to">s</a>
                    <a href="#" class="scroll-to">t</a>
                    <a href="#" class="scroll-to">u</a>
                    <a href="#" class="scroll-to">v</a>
                    <a href="#" class="scroll-to">w</a>
                    <a href="#" class="disable">z</a>
                </div>
                <div class="car-name-wrapper">
                    <div class="car-name">
                        <div class="litera"><p></p></div>
                        <div class="model"><h3>Марка и модель</h3></div>
                        <div class="year"><h3>Год</h3></div>
                    </div>
                    <div class="car-name" id="lit-a">
                        <div class="litera"><p>A</p></div>
                        <div class="model">
                            <p>Acura CL	        </p>
                            <p>Acura MDX	        </p>
                            <p>Acura RDX	        </p>
                            <p>Acura TL	        </p>
                            <p>Acura TSX	        </p>
                            <p>Audi A2	            </p>
                            <p>Audi A3	            </p>
                            <p>Audi A4	            </p>
                            <p>Audi A4 Avant	    </p>
                            <p>Audi A5	            </p>
                            <p>Audi A5 Avant	    </p>
                            <p>Audi A6	            </p>
                            <p>Audi A6 Avant	    </p>
                            <p>Audi A7	            </p>
                            <p>Audi A8	            </p>
                            <p>Audi A8 L	        </p>
                            <p>Audi Allroad	    </p>
                            <p>Audi Q3	            </p>
                            <p>Audi Q5	            </p>
                            <p>Audi Q7	            </p>
                            <p>Audi RS 3	        </p>
                            <p>Audi RS 4	        </p>
                            <p>Audi RS 4 Cabriolet	</p>
                            <p>Audi RS 5	        </p>
                            <p>Audi RS 6	        </p>
                            <p>Audi RS 7	        </p>
                            <p>Audi RS Q3	        </p>
                            <p>Audi S3	            </p>
                            <p>Audi S4	            </p>
                            <p>Audi S5	            </p>
                            <p>Audi S6	            </p>
                            <p>Audi S7	            </p>
                            <p>Audi S8	            </p>
                            <p>Audi SQ5	        </p>
                            <p>Aston Martin DB9    </p>
                            <p>Alfa Romeo 147	    </p>
                            <p>Alfa Romeo 156	    </p>
                            <p>Alfa Romeo 159	    </p>
                            <p>Alfa Romeo Giulietta</p>
                        </div>
                        <div class="year">
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2003</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2003</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-b">
                        <div class="litera"><p>B</p></div>
                        <div class="model">
                            <p>BYD F6	                  </p>
                            <p>Bentley Arnage	          </p>
                            <p>Bentley Azure	          </p>
                            <p>Bentley Bentayga	      </p>
                            <p>Bentley Brooklands	      </p>
                            <p>Bentley Continental	      </p>
                            <p>Bentley Flying Spur	      </p>
                            <p>Bentley Mulsanne	      </p>
                            <p>BMW 1-series	          </p>
                            <p>BMW 2-series Active Tourer</p>
                            <p>BMW 2-series Gran Tourer</p>
                            <p>BMW 3-series	          </p>
                            <p>BMW 3-series Gran Turismo</p>
                            <p>BMW 4-series Gran Coupe	  </p>
                            <p>BMW 5-series	          </p>
                            <p>BMW 5-series Gran Turismo</p>
                            <p>BMW 6-series	          </p>
                            <p>BMW 6-series Gran Coupe	  </p>
                            <p>BMW 7-series	          </p>
                            <p>BMW ActiveHybrid 5	      </p>
                            <p>BMW Alpina B7	          </p>
                            <p>BMW M5	                  </p>
                            <p>BMW M6	                  </p>
                            <p>BMW M6 Gran Coupe	      </p>
                            <p>BMW X1	                  </p>
                            <p>BMW X3	                  </p>
                            <p>BMW X4	                  </p>
                            <p>BMW X5	                  </p>
                            <p>BMW X6	                  </p>
                            <p>BMW i3	                  </p>
                        </div>
                        <div class="year">
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2003</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2003</p>
                            <p>2005</p>
                            <p>2005</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-c">
                        <div class="litera"><p>C</p></div>
                        <div class="model">
                            <p>Chevrolet Captiva	     </p>
                            <p>Chevrolet Cobalt	     </p>
                            <p>Chevrolet Cruze	         </p>
                            <p>Chevrolet Epica	         </p>
                            <p>Chevrolet Evanda	     </p>
                            <p>Chevrolet Malibu	     </p>
                            <p>Chevrolet Orlando	     </p>
                            <p>Chevrolet Tahoe	         </p>
                            <p>Chevrolet Tracker	     </p>
                            <p>Chevrolet Trailblazer	 </p>
                            <p>Cadillac ATS	         </p>
                            <p>Cadillac BLS	         </p>
                            <p>Cadillac CTS	         </p>
                            <p>Cadillac DTS	         </p>
                            <p>Cadillac Escalade	     </p>
                            <p>Cadillac SRX	         </p>
                            <p>Cadillac STS	         </p>
                            <p>Cadillac Seville	     </p>
                            <p>Cadillac XTS	         </p>
                            <p>Chrysler 200	         </p>
                            <p>Chrysler 300	         </p>
                            <p>Chrysler Grand Voyager	 </p>
                            <p>Chrysler PT Cruiser	     </p>
                            <p>Chrysler Pacifica	     </p>
                            <p>Chrysler Sebring	     </p>
                            <p>Chrysler Town and Country</p>
                            <p>Chrysler Voyager	     </p>
                            <p>Citroen C-Crosser	     </p>
                            <p>Citroen C-Elysee	     </p>
                            <p>Citroen C4	             </p>
                            <p>Citroen C4 Cactus	     </p>
                            <p>Citroen C4 Picasso	     </p>
                            <p>Citroen C5	             </p>
                            <p>Citroen C6	             </p>
                            <p>Citroen C8	             </p>
                            <p>Citroen DS4	             </p>
                            <p>Citroen DS5	             </p>
                            <p>Citroen DS6	             </p>
                            <p>Citroen Elysee	         </p>
                            <p>Citroen Grand Picasso	 </p>
                            <p>Chang'an CS35	         </p>
                            <p>Chang'an Eado	         </p>
                            <p>Chang'an Honor	         </p>
                        </div>
                        <div class="year">
                            <p>2005</p>
                            <p>2012</p>
                            <p>2015</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2008</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>	2005</p>
                            <p>2008</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>2008</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2012</p>
                            <p>2012</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-d">
                        <div class="litera"><p>D</p></div>
                        <div class="model">
                            <p>Dongfeng H30	   </p>
                            <p>Dongfeng S30	   </p>
                            <p>Dongfeng X30	   </p>
                            <p>Dacia Duster	   </p>
                            <p>Dodge Avenger	   </p>
                            <p>Dodge Caliber	   </p>
                            <p>Dodge Caravan	   </p>
                            <p>Dodge Charger	   </p>
                            <p>Dodge Grand Caravan</p>
                            <p>Dodge Journey	   </p>
                            <p>Dodge Nitro	       </p>
                        </div>
                        <div class="year">
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>	2012</p>
                            <p>2005</p>
                            <p>2005</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-f">
                        <div class="litera"><p>F</p></div>
                        <div class="model">
                            <p>Ford C-Max	     </p>
                            <p>Ford C-Max Hybrid</p>
                            <p>Ford EcoSport	 </p>
                            <p>Ford Edge	     </p>
                            <p>Ford Escape	     </p>
                            <p>Ford Explorer	 </p>
                            <p>Ford Fiesta	     </p>
                            <p>Ford Five Hundred</p>
                            <p>Ford Focus	     </p>
                            <p>Ford Fusion	     </p>
                            <p>Ford Galaxy	     </p>
                            <p>Ford Grand C-Max</p>
                            <p>Ford Kuga	     </p>
                            <p>Ford Maverick	 </p>
                            <p>Ford Mondeo	     </p>
                            <p>Ford S-Max	     </p>
                            <p>Ford Taurus	     </p>
                            <p>Ford Tourneo	 </p>
                            <p>Ford Vignale	 </p>
                            <p>Fiat 500L	     </p>
                            <p>Fiat 500X	     </p>
                            <p>Fiat Croma	     </p>
                            <p>Fiat Freemont	 </p>
                            <p>Fiat Linea	     </p>
                            <p>Fiat Qubo	     </p>
                            <p>FAW Besturn B50	 </p>
                        </div>
                        <div class="year">
                            <p>2005</p>
                            <p>	2005</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>	2005</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>	2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2010</p>
                            <p>2005</p>
                            <p>2015</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-g">
                        <div class="litera"><p>G</p></div>
                        <div class="model">
                            <p>Great Wall Haval H3</p>
                            <p>Great Wall Haval H5</p>
                            <p>Great Wall Haval M2</p>
                            <p>Great Wall Haval M4</p>
                            <p>Great Wall M4	  </p>
                        </div>
                        <div class="year">
                            <p>	2012</p>
                            <p>	2012</p>
                            <p>	2012</p>
                            <p>	2012</p>
                            <p>2015</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-h">
                        <div class="litera"><p>H</p></div>
                        <div class="model">
                            <p>Hummer H2</p>
                            <p>Hummer H3</p>
                            <p>Hyundai Accent	      </p>
                            <p>Hyundai Avante	      </p>
                            <p>Hyundai Azera	      </p>
                            <p>Hyundai Elantra	      </p>
                            <p>Hyundai Equus	      </p>
                            <p>Hyundai Genesis	      </p>
                            <p>Hyundai Grand Santa Fe</p>
                            <p>Hyundai Grandeur	  </p>
                            <p>Hyundai H-1	          </p>
                            <p>Hyundai Ioniq	      </p>
                            <p>Hyundai Santa Fe	  </p>
                            <p>Hyundai Santa Fe Sport</p>
                            <p>Hyundai Solaris	      </p>
                            <p>Hyundai Sonata	      </p>
                            <p>Hyundai Terracan	  </p>
                            <p>Hyundai Tucson	      </p>
                            <p>Hyundai Veracruz	  </p>
                            <p>Hyundai i30	          </p>
                            <p>Hyundai i35	          </p>
                            <p>Hyundai i40	          </p>
                            <p>Hyundai i45	          </p>
                            <p>Hyundai ix35	      </p>
                            <p>Hyundai ix55	      </p>
                            <p>Honda Accord	</p>
                            <p>Honda CR-V	    </p>
                            <p>Honda City	    </p>
                            <p>Honda Civic	    </p>
                            <p>Honda Crosstour	</p>
                            <p>Honda FR-V	    </p>
                            <p>Honda HR-V	    </p>
                            <p>Honda Insight	</p>
                            <p>Honda Jazz	    </p>
                            <p>Honda Legend	</p>
                            <p>Honda Odyssey	</p>
                            <p>Honda Pilot	    </p>
                            <p>Honda Stream	</p>
                        </div>
                        <div class="year">
                            <p>2005</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>	2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>	2005</p>
                            <p>2015</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2015</p>
                            <p>2005</p>
                            <p>2012</p>
                            <p>2005</p>
                            <p>2005</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-i">
                        <div class="litera"><p>I</p></div>
                        <div class="model">
                            <p>Infiniti ESQ	</p>
                            <p>Infiniti EX	    </p>
                            <p>Infiniti FX     </p>
                            <p>Infiniti G Sedan</p>
                            <p>Infiniti M	    </p>
                            <p>Infiniti Q40	</p>
                            <p>Infiniti Q45	</p>
                            <p>Infiniti Q50	</p>
                            <p>Infiniti Q50L	</p>
                            <p>Infiniti Q60	</p>
                            <p>Infiniti Q70	</p>
                            <p>Infiniti QX4	</p>
                            <p>Infiniti QX50	</p>
                            <p>Infiniti QX56	</p>
                            <p>Infiniti QX60	</p>
                            <p>Infiniti QX70	</p>
                            <p>Infiniti QX80	</p>
                        </div>
                        <div class="year">
                            <p>2005</p>
                            <p>2005</p>
                            <p>2003</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-j">
                        <div class="litera"><p>J</p></div>
                        <div class="model">















                            <p>Jaguar E-Type	   </p>
                            <p>Jaguar F-Type	   </p>
                            <p>Jaguar S-Type	   </p>
                            <p>Jaguar X-Type	   </p>
                            <p>Jaguar XE	       </p>
                            <p>Jaguar XF	       </p>
                            <p>Jaguar XJ	       </p>
                            <p>Jaguar XJR	       </p>
                            <p>Jeep Cherokee	   </p>
                            <p>Jeep Commander	   </p>
                            <p>Jeep Compass	   </p>
                            <p>Jeep Grand Cherokee</p>
                            <p>Jeep Liberty	   </p>
                            <p>Jeep Patriot	   </p>
                            <p>Jeep Renegade	   </p>
                        </div>
                        <div class="year">
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2003</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                            <p>2005</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-l">
                        <div class="litera"><p>L</p></div>
                        <div class="model">
                            <p>Land Rover Range Rover</p>
                            <p>Lexus ES</p>
                            <p>Lexus GS</p>
                            <p>Lexus GX</p>
                            <p>Lexus IS</p>
                            <p>Lexus LS</p>
                        </div>
                        <div class="year">
                            <p>2015</p>
                            <p>2011</p>
                            <p>2007</p>
                            <p>2013</p>
                            <p>2011</p>
                            <p>2007</p>
                        </div>
                    </div>

                    <div class="car-name" id="lit-v">
                        <div class="litera"><p>V</p></div>
                        <div class="model">
                            <p>Volvo S80</p>
                            <p>Volvo Stretch Limo</p>
                            <p>Volkswagen Arteon</p>
                            <p>Volkswagen CC</p>
                            <p>Volkswagen Passat</p>
                            <p>Volkswagen Phaeton</p>
                        </div>
                        <div class="year">
                            <p>2011</p>
                            <p>2011</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-n">
                        <div class="litera"><p>N</p></div>
                        <div class="model">
                            <p>Nissan Infiniti</p>
                        </div>
                        <div class="year">
                            <p>2011</p>
                        </div>
                    </div>
                    <div class="car-name" id="lit-t">
                        <div class="litera"><p>T</p></div>
                        <div class="model"><p>Toyota Camry</p></div>
                        <div class="year"><p>2014</p></div>
                    </div>
                    <div class="car-name" id="lit-v">
                        <div class="litera"><p>V</p></div>
                        <div class="model">

                        </div>
                        <div class="year">
                            <p>2017</p>
                            <p>2014</p>
                            <p>2015</p>
                            <p>2011</p>
                        </div>
                    </div>
                    <div class="car-name">
                        <div class="litera"><p>M</p></div>
                        <div class="model">
                            <p>Mercedes-Benz C-Class</p>
                            <p>Mercedes-Benz CLS-Class</p>
                            <p>Mercedes-Benz E-Class</p>
                            <p>Mercedes-Benz GL-Class</p>
                            <p>Mercedes-Benz GLE-Class</p>
                            <p>Mercedes-Benz S-Class</p>
                        </div>
                        <div class="year">
                            <p>2011</p>
                            <p>2006</p>
                            <p>2009 w212</p>
                            <p>2015</p>
                            <p>2015</p>
                            <p>2006</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>