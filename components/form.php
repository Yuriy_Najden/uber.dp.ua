<form role="form">
    <div class="form-group">
        <input type="text" name="name" class="form-control" placeholder="Имя" required>
    </div>
    <div class="form-group">
        <input type="tel" name="phone" class="form-control" placeholder="Телефон" required>
    </div>
    <div class="form-group">
        <input type="email" name="email" class="form-control" placeholder="Email" required>
    </div>
    <input type="hidden" name="formName" value="">
    <button type="submit" class="button">Отправить</button>
</form>