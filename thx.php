<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.png" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#000000">

    <meta name="theme-color" content="#000000">

    <title>Спасибо!</title>

    <link rel="stylesheet" href="css/likely.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css"/>


</head>
<body>
<div class="wrapper thx-page">
    <header>
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-2 col-sm-3 col-4 logo">
                    <img src="img/logo.svg" alt="">
                </div>

                <div class="col-xl-2 col-md-5 phone col-sm-4 col-5 text-right">
                    <a href="tel:380636718020">
                        <img src="img/phone-marker.svg" alt="" class="d-none">
                        <p>+380 63 671 80 20</p>
                    </a>
                </div>
            </div>
        </div>
    </header>

    <section class="top-baner">
        <div class="container">
            <div class="row">
                <div class="text col-lg-6">
                    <h1>Спасибо что оставили заявку на работу в Uber</h1>
                    <p>Мы свяжемся с вами в течение дня.</p>
                    <a href="index.php" class="button">Вернуться на сайт</a>
                </div>
                <div class="pic col-lg-6">
                    <img src="img/header-pic.jpg" alt="">
                </div>

            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content">
                        <p>© 2018 UberDnipro. Все права защищены.</p>
                        <p>Разработано в <a href="https://smmstudio.com.ua/" target="_blank">SMMSTUDIO</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACxR3qD-AgGQmQeiYC4YMwHdoKTZj5TT0"></script> <!--&callback=initMap -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!--<script src="js/jquery-3.2.1.min.js"></script>-->

<script src="js/bootstrap.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/jquery.viewportchecker.min.js"></script>
<script src="js/js.js"></script>

</body>
</html>
