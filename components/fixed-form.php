<div class="fixed-form-veil"></div>

<div class="fixed-form">
    <a href="#" class="close-btn">
        <svg width="27" height="26" viewBox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="23" width="5" height="31" transform="rotate(45 23 0)" fill="white"/>
            <path d="M26 21.9199L22.4645 25.4555L0.544156 3.53515L4.07969 -0.000387967L26 21.9199Z" fill="white"/>
        </svg>
    </a>
    <div class="content-wrapper">
        <h2 class="block-title">Стать водителем</h2>
        <h3>Заполните форму и присоединитесь ко всемирно известному сервису такси Uber в качестве профессионального
            водителя Uber в Днепре
        </h3>
        <div class="form-wrapper">
            <form method="post" action="send.php">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Имя" required>
                </div>
                <div class="form-group">
                    <input type="text" name="firstName" class="form-control" placeholder="Фамилия" required>
                </div>
                <div class="form-group">
                    <input type="tel" name="phone" class="form-control" placeholder="Телефон" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label class="file-wrapper">
                        <input name="file" type="file" class="file-upload-input" required multiple>

                        <img src="img/mor-doc.svg" alt=""><span>Прикрепить документы</span>

                        <div id="fl_nm"><p></p></div>
                    </label>
                </div>

                <div class="button-wrapper">
                    <button type="submit" class="button">Оставить заявку</button>
                    <p class="small ">Данные не будут переданы третьим лицам</p>
                </div>

                <p class="small">Важно! Все фото должны быть четкими, с видимыми краями документов. Все стороны
                    документов должны быть прикреплены отдельными фотографиями.
                </p>

            </form>
        </div>
    </div>
</div>