$(function() {

    // ANIMATE INIT

    //new WOW().init();

    // MAP INIT

    //$(function() {
//
//
    //    function initMap() {
    //        var location = {
    //            lat: 48.268376,
    //            lng: 25.9301257
    //        };
//
    //        var map = new google.maps.Map(document.getElementById('map'), {
    //            zoom: 15,
    //            center: location,
    //            scrollwheel: false
    //        });
//
    //        var marker = new google.maps.Marker({
    //            position: location,
    //            map: map
    //        });
//
    //    }
//
    //    initMap();
//
    // MOB-MENU

    //$("#mob-menu").on("click", function(){
    //    $("#nav-menu").slideToggle();
    //    $(this).toggleClass("active-mobile-menu");
    //    $('#nav').toggleClass('menu-in');
    //    $("#nav-menu a").on("click", function () {
    //        $("#nav-menu").slideToggle();
    //        $("#mob-menu").removeClass("active-mobile-menu");
    //    });
    //});

    // SLIDER INIT

    //$('#type-slide-1').slick({
    //    autoplay: true,
    //    arrows: false,
    //    dots: true,
    //    slidesToShow: 1,
    //    centerMode: false,
    //    draggable: false,
    //    infinite: true,
    //    pauseOnHover: true,
    //    swipe: false,
    //    touchMove: false,
    //    fade:true,
    //    autoplaySpeed: 5000,
    //    adaptiveHeight: true
    //});

    // SHOW SCROLL TOP BUTTON.

    /*$(document).scroll(function() {
        var y = $(this).scrollTop();

        var picAnimation2 = $('.requirement').offset().top;
        var picAnimation1 = $('.customers-choose').offset().top;

        if(  y = picAnimation2 ){
            $('.requirement .pic-animation-wrapper').addClass('pic-animation');
        }

        if( y = picAnimation1 ){
            $('.customers-choose .pic-animation-wrapper').addClass('pic-animation');
        }
    });*/

    // SCROLL TOP.

    //$(document).on('click', '.up-btn', function() {
    //    $('html, body').animate({
    //        scrollTop: 0
    //    }, 300);
    //});

    //SCROLL MENU

   $(document).on('click', '.scroll-to', function (e) {
       e.preventDefault();

       var href = $(this).attr('href');

       $('html, body').animate({
           scrollTop: $(href).offset().top
       }, 1000);

   });

    // POPUPS

    //$(document).on('click', '.open-popup', function (e) {
    //    e.preventDefault();
//
    //    var popup = $(this).data('popup');
//
    //    $(popup).fadeIn();
    //});
//
    //$(document).on('click', '.close-popup', function (e) {
    //    e.preventDefault();
//
    //    $('.popup').fadeOut();
    //});

    // PRELOADER

    //$(window).on('load', function () {
    //    var $preloader = $('#preloader');
//
    //    $preloader.delay(500).fadeOut('slow');
    //});

    // CASTOME SLIDER ARROWS

   //$('.mein-slider').slick({
   //    autoplay: false,
   //    autoplaySpeed: 5000,
   //    slidesToShow: 1,
   //    slidesToScroll: 1,
   //    arrows: false,
   //    fade: true

   //});

   //$('.main-page .arrow-left').click(function(){
   //    $('.mein-slider').slick('slickPrev');
   //});

   //$('.main-page .arrow-right').click(function(){
   //    $('.mein-slider').slick('slickNext');
   //});

    // FORM LABEL FOCUS UP

    //$('.form-input').on('focus', function() {
    //    $(this).closest('.input-fild').find('label').addClass('active');
    //});
    //$('.form-input').on('blur', function() {
    //    var $this = $(this);
    //    if ($this.val() == '') {
    //        $this.closest('.input-fild').find('label').removeClass('active');
    //    }
    //});

    // DTA VALUE REPLACE

    //$('.open-form').on('click', function (e) {
    //    e.preventDefault();
    //    var type = $(this).data('type');
    //    $('#type-form').find('input[name=type]').val(type);
    //});

    // PHONE MASK

    $("input[type=tel]").mask("+38(999) 999-99-99");

    //

    $(".fui1").change(function() {
        var f_name1 = [];

        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            f_name1.push(" " + $(this).get(0).files[i].name);
            $("#fl_nm1 p").text(f_name1);
        }

    });

    $(".fui2").change(function() {
        var f_name2 = [];

        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            f_name2.push(" " + $(this).get(0).files[i].name);
            $("#fl_nm2 p").text(f_name2);
        }
    });

    $(".fui3").change(function() {
        var f_name3 = [];

        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            f_name3.push(" " + $(this).get(0).files[i].name);
            $("#fl_nm3 p").text(f_name3);
        }

    });

    $(".fui4").change(function() {
        var f_name4 = [];

        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            f_name4.push(" " + $(this).get(0).files[i].name);
            $("#fl_nm4 p").text(f_name4);
        }

    });

    //

    $('#open-more-forms').on('click', function (e) {
        e.preventDefault();

        $('#closer-more-forms').removeClass('open-more');
        $(this).addClass('open-more');

        $('.more-form-content').slideToggle(400);

    });

    $('#closer-more-forms').on('click', function (e) {
        e.preventDefault();

        $('#open-more-forms').removeClass('open-more');
        $(this).addClass('open-more');

        $('.more-form-content').slideToggle(400);

    });

    //

    $('.pic-animation-wrapper').viewportChecker({
        classToAdd: 'pic-animation'
    });

    //

    $('.open-form').on('click', function (e) {
        e.preventDefault();

        $('.fixed-form-veil').fadeIn(500);

        setTimeout( function () {
            $('.fixed-form').addClass('active-form');
        }, 1000);
    });

    $('.fixed-form .close-btn, .fixed-form-veil').on('click', function (e) {
        e.preventDefault();

        setTimeout( function () {
            $('.fixed-form').removeClass('active-form');
        }, 500);

        $('.fixed-form-veil').fadeOut(1500);
    });
});

$(window).on('load', function () {

    'use strict';

    setTimeout(function() {
        $('.header-content').addClass('page-ready');
    }, 4000);

});
