<!DOCTYPE html>
<html lang="ru">
  <head>
   
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.png" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

      <meta name="msapplication-TileColor" content="#000000">

      <meta name="theme-color" content="#000000">

    <title>uber</title>

      <link rel="stylesheet" href="css/likely.css">
      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link type="text/css" rel="stylesheet" href="css/style.css"/>


  </head>
  <body>
    <div class="wrapper">
        <header>
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-2 col-sm-3 col-4 logo">
                        <img src="img/logo.svg" alt="">
                    </div>

                    <div class="col-xl-4 col-md-5 phone col-sm-4 col-5">
                        <a href="tel:380636718020">
                            <img src="img/phone-marker.svg" alt="" class="d-md-none">
                            <p>+380 63 671 80 20</p>
                        </a>
                        <a href="#contact-form" class="driver scroll-to">Стать водителем</a>
                    </div>
                </div>
            </div>
        </header>

        <section class="top-baner">
            <div class="container">
                <div class="row">
                    <div class="text col-lg-6">
                        <h1>Приглашаем владельцев автомобилей на работу в Uber</h1>
                        <p>Зарабатывайте до 40 000 ₴ на своем авто! *</p>
                        <a href="#contact-form" class="button scroll-to">Оставить заявку</a>
                        <p class="small">*Сумма чистого дохода определяется индивидуально и зависит от работы каждого водителя.</p>
                    </div>
                    <div class="pic col-lg-6">
                        <img src="img/header-pic.jpg" alt="">
                    </div>

                </div>
            </div>
        </section>

        <section class="uber">
            <div class="container">
                <div class="row">
                    <div class="content col-12">
                        <div class="items-wrapper">
                            <img src="img/Uber-pic.svg" alt="" class="pic">
                            <p class="text">Официальный партнер в Днепре всемирно известной компании Uber.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="uber-advantage">
            <div class="content-aer">
                <div class="container">
                    <div class="row">
                        <h2 class="block-title col-12"></h2>
                        <div class="item col-lg-4 col-md-5 col-sm-6 col-12">
                            <img src="img/adv-pic-1.svg" alt="">
                            <h3>Гибкий график</h3>
                            <p>Вы сами решаете когда и сколько работать: полный рабочий день, “от случая к случаю” или же
                                просто в подрабатывать в свободное время.
                            </p>
                        </div>

                        <div class="item col-lg-4 offset-md-2 col-md-5 col-sm-6 col-12">
                            <img src="img/adv-pic-2.svg" alt="">
                            <h3>Простая регистрация</h3>
                            <p>Вам не нужно открывать ФЛП или ООО - заполните форму, прикрепите необходимые документы и
                                дождитесь подтверждения от партнера Uber.
                            </p>
                        </div>

                        <div class="item col-lg-4 col-md-5 col-sm-6 col-12">
                            <img src="img/adv-pic-3.svg" alt="">
                            <h3>Ежедневные заказы</h3>
                            <p>Приложением ежедневно пользуются десятки тысяч украинцев. А иностранцы предпочитают заказать
                                машину через знакомое им приложение.
                            </p>
                        </div>

                        <div class="item col-lg-4 offset-md-2 col-md-5 col-sm-6 col-12">
                            <img src="img/adv-pic-4.svg" alt="">
                            <h3>Приятные  бонусы</h3>
                            <p>Uber всегда поощряет хорошую работу своих водителей. Компания проводит бонусные программы
                                и акции, чтобы водители могли заработать больше.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="content col-12">
                            <a href="#contact-form" class="button scroll-to">Стать водителем</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="why-we">
            <div class="container">
                <div class="row">
                    <h2 class="block-title col-12">Почему именно UberDnipro</h2>
                    <div class="item-wrapper col-12">
                        <div class="text">
                            <p class="item">Вы можете начать работать с первого дня старта UberDnipro в Днепре.</p>
                            <p class="item">Самая низкая комиссия среди партнеров Uber — всего 5%!</p>
                            <p class="item">Гарантируем компенсации за отмены заказов и еженедельные своевременные выплаты.</p>
                            <p class="item">Мы заботимся о наших водителях и предоставляем круглосуточную поддержку 24/7.</p>
                        </div>
                        <div class="pic-animation-wrapper">
                            <img src="img/woy-we-pic.jpg" alt="" class="pic">
                            <span class="pic-visible-animation"></span>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="popularity">
            <div class="container">
                <div class="row">
                    <h2 class="block-title col-12">Популярность Uber среди клиентов</h2>
                    <div class="item col-lg-5 col-sm-6">
                        <span>01</span>
                        <p>На данный момент сервис работает в более чем 70 странах, 300 городах и имеет 50 млн. постоянных
                            пользователей по всему миру.
                        </p>
                    </div>
                    <div class="item col-lg-5 offset-lg-1 col-sm-6">
                        <span>02</span>
                        <p>Создав удобное и простое в использовании приложение для вызова машины Uber составил серьезную
                            конкуренцию службам такси офлайн.
                        </p>
                    </div>
                    <div class="item col-lg-5 col-sm-6">
                        <span>03</span>
                        <p>Uber быстро занял лидирующие позиции среди всех служб заказа такси благодаря исключительной
                            клиенториентированности и прозрачности оплаты.
                        </p>
                    </div>
                    <div class="item col-lg-5 offset-lg-1 col-sm-6">
                        <span>04</span>
                        <p>Заказывая такси в приложении Uber клиент может отследить путь автомобиля в режиме реального
                            времени и произвести оплату онлайн.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="pictures">
            <div class="pic-wrapper">
                <div class="pic-animation-wrapper">
                    <img src="img/requirement-pic-1.jpg" alt="" class="pic">
                    <span class="pic-visible-animation"></span>
                </div>
                <div class="pic-animation-wrapper">
                    <img src="img/requirement-pic-2.jpg" alt="" class="pic">
                    <span class="pic-visible-animation"></span>
                </div>
            </div>
        </section>

        <section class="our-driver">
            <div class="container">
                <div class="row">
                    <h2 class="block-title col-12">Водитель, которого мы ищем</h2>
                    <ul class="col-xl-9 col-lg-11 col-12">
                        <li>гражданин Украины старше 19 лет</li>
                        <li>есть водительское удостоверение</li>
                        <li>владелец автомобиля из <a href="#" target="_blank">списка Uber</a></li>
                        <li>реальный стаж вождения не менее года</li>
                        <li>есть телефон или планшет с 3G интернетом</li>
                        <li>готов зарабатывать на своем авто</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-12">
                        <a href="#contact-form" class="button scroll-to">Стать водителем</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="uber-black">
            <div class="container">
                <div class="row">
                    <div class="content col-12">
                        <h2>Открыта предварительная регистрация для владельцев автомобилей премиум-класса на первый в Днепре UberBLACK</h2>
                        <p>Высокий тариф. Низкая комиссия. Гибкий график.</p>
                        <a href="https://bleck.uber.dp.ua" class="button" target="_blank">Узнать больше</a>
                        <img src="img/ubl-pic.jpg" alt="" class="pic">
                    </div>
                </div>
            </div>
        </section>

        <!--<section class="documents">
            <div class="content-aer">
                <div class="container">
                    <div class="row">
                        <h2 class="block-title col-12">Документы для подключения</h2>
                        <div class="item-wrapper col-xl-9">
                            <div class="item">
                                <h4>Паспорт или ВНЖ Украины</h4>
                                <p>Фото 1-ой, 2-ой страниц и прописки</p>
                            </div>
                            <div class="item">
                                <h4>Свидетельство о регистрации ТС</h4>
                                <p>Фото обеих сторон документа</p>
                            </div>
                            <div class="item">
                                <h4>Водительское удостоверение</h4>
                                <p>Фото обеих сторон документа</p>
                            </div>
                            <div class="item">
                                <h4>Актуальная страховка</h4>
                                <p>Действующий страховой полис</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->

        <section class="driver-steps">
            <div class="container">
                <div class="row">
                    <h2 class="block-title col-12">Как стать водителем Uber в Днепре</h2>
                    <div class="item-wrapper col-xl-11">
                        <div class="item">
                            <p class="number">01</p>
                            <p class="text">Убедитесь, что вы и ваш автомобиль соответствует всем требованиям</p>
                        </div>
                        <div class="item">
                            <p class="number">02</p>
                            <p class="text">Заполните форму, прикрепите документы  и забронируйте место</p>
                        </div>
                        <div class="item">
                            <p class="number">03</p>
                            <p class="text">Станьте одним из первых водителей UberBlack в Днепре!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="contact-form" id="contact-form">
            <div class="container">
                <div class="row">
                    <div class="content col-12">
                        <h2 class="block-title">Стать водителем</h2>
                        <h3>Заполните форму для обратной связи с нашим менеджером или заполните полную анкету водителя*
                            и присоединитесь ко всемирно известному сервису такси Uber.
                        </h3>
                        <div class="form-wrapper">
                            <form method="post" action="send.php">
                                <div class="paralel">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="Имя" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="firstName" class="form-control" placeholder="Фамилия" required>
                                    </div>
                                </div>
                                <div class="paralel">
                                    <div class="form-group">
                                        <input type="tel" name="phone" class="form-control" placeholder="Телефон" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                </div>


                                <div class="mor-btn-wrapper">
                                    <a href="#" id="open-more-forms">да</a>
                                    <a href="#" class="open-more" id="closer-more-forms">нет</a>
                                    <p>Заполнить анкету водителя</p>
                                </div>
                                <div class="more-form-content">
                                    <div class="paralel">
                                        <div class="form-group">
                                            <input type="text" name="carName" class="form-control" placeholder="Марка авто">
                                        </div>
                                        <div class="form-group">
                                            <input type="text"  name="carYear" class="form-control" placeholder="Год выпуска">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="carNumber" class="form-control" placeholder="Номер авто">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="cardNumber"  class="form-control" placeholder="Банковская карта">
                                    </div>
                                    <select name="banckName" class="form-control banck-name">
                                        <option disabled="disabled" >Название банка и тип карточки</option>
                                        <option value="Приватбанк, карта «Универсальная»">Приватбанк, карта «Универсальная»</option>
                                        <option value="Приватбанк, карта для выплат">Приватбанк, карта для выплат</option>
                                        <option value="Альфа-Банк">Альфа-Банк</option>
                                        <option value="Другой">Другой</option>
                                    </select>
                                    <div class="form-group file-group">
                                        <label class="file-wrapper fw1">
                                            <p class="file-name-type">Фото** водительского удостоверения 1 сторона</p>
                                            <input name="file-passport-1" id="file-passport-1" type="file" class="file-upload-input fui1">

                                            <span>выбрать файл</span>

                                            <div id="fl_nm1"><p>Файл не выбран</p></div>
                                        </label>
                                    </div>
                                    <div class="form-group file-group">
                                        <label class="file-wrapper fw2">
                                            <p class="file-name-type">Фото** водительского удостоверения 2 сторона</p>
                                            <input name="file-passport-2" id="file-passport-2" type="file" class="file-upload-input fui2">

                                            <span>выбрать файл</span>

                                            <div id="fl_nm2"><p>Файл не выбран</p></div>
                                        </label>
                                    </div>
                                    <div class="form-group file-group">
                                        <label class="file-wrapper fw3">
                                            <p class="file-name-type">Фото свидетельства о регистрации авто</p>
                                            <input name="file-car-reg" id="file-car-reg" type="file" class="file-upload-input fui3">

                                            <span>выбрать файл</span>

                                            <div id="fl_nm3"><p>Файл не выбран</p></div>
                                        </label>
                                    </div>
                                    <div class="form-group file-group">
                                        <label class="file-wrapper fw4">
                                            <p class="file-name-type">Фото страхового полиса автоцивилки</p>
                                            <input name="file-car-belay" id="file-car-belay" type="file" class="file-upload-input fui4">

                                            <span>выбрать файл</span>

                                            <div id="fl_nm4"><p>Файл не выбран</p></div>
                                        </label>
                                    </div>
                                    <p class="small">* Данные не будут переданы третьим лицам</p>
                                    <p class="small">** Важно! Все фото должны быть четкими, с видимыми краями документов.
                                        Все стороны документов должны быть прикреплены отдельными фотографиями.
                                    </p>
                                </div>

                                <div class="button-wrapper">
                                    <button type="submit" class="button">Оставить заявку</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content">
                            <p>© 2018 UberDnipro. Все права защищены.</p>
                            <p>Разработано в <a href="https://smmstudio.com.ua/" target="_blank">SMMSTUDIO</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>

    <?php include ('components/popup.php');?>


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACxR3qD-AgGQmQeiYC4YMwHdoKTZj5TT0"></script> <!--&callback=initMap -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!--<script src="js/jquery-3.2.1.min.js"></script>-->

    <script src="js/bootstrap.min.js"></script>

    <script src="js/wow.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/js.js"></script>
    
  </body>
</html>
