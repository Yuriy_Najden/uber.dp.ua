<?php
session_start();

require_once('classes/AmoCrm.php');

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name       = clearData(($_POST['name']));
$email      = clearData($_POST['email']);
$phone      = clearData($_POST['phone']);
$firstName  = clearData(($_POST['firstName']));
$carName    = clearData($_POST['carName']);
$carYear    = clearData($_POST['carYear']);
$carNumber  = clearData($_POST['carNumber']);
$cardNumber = clearData($_POST['cardNumber']);
$banckName  = clearData($_POST['banckName']);

$utmSource   = clearData($_SESSION['utm_source']);
$utmMedium   = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm     = clearData($_SESSION['utm_term']);
$utmContent  = clearData($_SESSION['utm_content']);

if ($_FILES) {
    $target_dir = "uploads/";

    $target_file1 = $target_dir . basename($_FILES["file-passport-1"]["name"]);
    $target_file2 = $target_dir . basename($_FILES["file-passport-2"]["name"]);
    $target_file3 = $target_dir . basename($_FILES["file-car-reg"]["name"]);
    $target_file4 = $target_dir . basename($_FILES["file-car-belay"]["name"]);
}


if(!empty($name) && !empty($phone)) {

    // Save user in crm
    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'example@example.com.ua',
        'USER_HASH'  => 'example'
    ], 'example');

    $lead = $amoCrm->storeLead('Имя Заявки', 20308306, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $promo);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);

    header('Location: thx.php');

} else {

    die('Data is empty!');

}